//package by.bsuir.rekishbank;
//
//import by.bsuir.rekishbank.entity.*;
//import by.bsuir.rekishbank.service.*;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import static org.junit.Assert.*;
//
//import javax.transaction.Transactional;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Random;
//
//@SpringBootTest
////@Transactional
//class RekishBankApplicationTests {
//
//    @Autowired
//    private CurrencyService currencyService;
//
//    @Autowired
//    private ContributionService contributionService;
//
//    @Autowired
//    private CardService cardService;
//
//    @Autowired
//    private RoleService roleService;
//
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private ClientService clientService;
//
//    @Autowired
//    private AccountService accountService;
//
//    private Random random = new Random();
//
//    @Test
////    @Transactional
//    void contextLoads() {
//        List<Currency> currencies = addTestCurrencies();
//        List<Contribution> contributions = addTestContributions();
//        List<Card> cards = addTestCards();
//        List<Role> roles = addTestRoles();
//        List<User> users = addTestUsers(roles);
//        List<Client> clients = addTestClients(users);
//        List<Account> accounts = addTestAccounts(currencies, clients, cards, contributions);
//
//        System.out.println("\n\nDONE\n\n");
//
//        System.out.println(accounts);
//
//
//
//    }
//
//    private List<Currency> addTestCurrencies() {
//        ArrayList<Currency> currencies = new ArrayList<>();
//        currencies.add(new Currency("USD Test", 1));
//        currencies.add(new Currency("BYN Test", 0.5));
//        currencies.add(new Currency("EUR Test", 1.1));
//        currencies.forEach(currencyService::addCurrency);
//        return currencies;
//    }
//
//
//    private List<Account> addTestAccounts(
//
//            List<Currency> currencies,
//            List<Client> clients,
//            List<Card> cards,
//            List<Contribution> contributions) {
//
//        ArrayList<Account> accounts = new ArrayList<>();
//        for (Client client : clients) {
//            Account newAccount = new Account(new Date(), new Date(), random.nextDouble() * 10000);
//
//            newAccount.setCurrency(currencies.get(random.nextInt(currencies.size())));
//
//            Card card = cards.get(random.nextInt(cards.size()));
//            card.setAccount(newAccount);
//            newAccount.setCard(card);
//
//            newAccount.setContribution(contributions.get(random.nextInt(contributions.size())));
//
//            newAccount.setClient(client);
//            client.setAccount(newAccount);
//
//            accounts.add(newAccount);
//        }
////        for (int i = 0; i < accounts.size(); i++) {
////            accounts.get(i).setCard(cards.get(i));
////        }
//        return accounts;
//    }
//
//    private List<Client> addTestClients(List<User> users) {
//        ArrayList<Client> clients = new ArrayList<>();
//        for (User user : users) {
//            if (!user.getRole().getName().equals("ADMIN")) {
//                int randomNumber = random.nextInt(100);
//                Client newClient = new Client(
//                        "testFirstName" + randomNumber,
//                        "testLastName" + randomNumber,
//                        "testPN" + randomNumber,
//                        "testPhone" + randomNumber,
//                        "testAddress" + randomNumber);
//                newClient.setUser(user);
//                clients.add(newClient);
//            }
//        }
//        clients.forEach(clientService::addClient);
//        return clients;
//    }
//
//    private List<User> addTestUsers(List<Role> roles) {
//        ArrayList<User> users = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            User user = new User("testuser" + i, Integer.toString(i), Integer.toString(i));
//            user.setRole(roles.get(i % 2));
//            users.add(user);
//        }
//        users.forEach(userService::addUser);
//        return users;
//    }
//
//    private List<Role> addTestRoles() {
//        ArrayList<Role> roles = new ArrayList<>();
//        roles.add(new Role("ADMIN"));
//        roles.add(new Role("USER"));
//        roles.forEach(roleService::addRole);
//        return roles;
//    }
//
//    private List<Card> addTestCards() {
//        ArrayList<Card> cards = new ArrayList<>();
//        cards.add(new Card(new Date(), 156));
//        cards.add(new Card(new Date(), 138));
//        cards.add(new Card(new Date(), 912));
//        cards.forEach(cardService::addCard);
//        return cards;
//    }
//
//
//
//
//    private List<Contribution> addTestContributions() {
//        ArrayList<Contribution> contributions = new ArrayList<>();
//        contributions.add(new Contribution(
//                "Test Contribution 1", 30, 2.2, true, true));
//        contributions.add(new Contribution(
//                "Test Contribution 2", 60, 2.4, true, false));
//        contributions.add(new Contribution(
//                "Test Contribution 3", 90, 2.9, false, true));
//        contributions.add(new Contribution(
//                "Test Contribution 4", 100, 2.9, false, false));
//        contributions.forEach(contributionService::addContribution);
//        return contributions;
//    }
//
//}
