package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Integer> {
    @Override
    Optional<Client> findById(Integer integer);
}
