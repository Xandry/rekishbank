package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
