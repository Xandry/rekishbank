package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
    @Override
    Optional<User> findById(Integer integer);

    Optional<User> findByUsername(String username);
}
