package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Currency;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CurrencyRepository extends CrudRepository<Currency, Integer> {
    Optional<Currency> findById(int id);
}
