package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Card;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CardRepository extends CrudRepository<Card, Integer> {
    //    Optional<Card> findByCardNumberAndCvcAndExpirationDate
    Optional<Card> getByCardNumber(Integer cardNumber);

    Optional<Card> getByCardNumberAndCvc(Integer cardNumber, Integer cvc);
}
