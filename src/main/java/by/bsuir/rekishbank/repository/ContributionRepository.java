package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Contribution;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ContributionRepository extends CrudRepository<Contribution, Integer> {
    @Override
    Optional<Contribution> findById(Integer integer);
}
