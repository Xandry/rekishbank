package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Integer> {
    @Override
    Optional<Account> findById(Integer integer);
}
