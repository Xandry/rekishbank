package by.bsuir.rekishbank.repository;

import by.bsuir.rekishbank.entity.Currency;
import by.bsuir.rekishbank.entity.CurrencyHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HistoryRepository extends CrudRepository<CurrencyHistory, Integer> {
    List<CurrencyHistory> findAllByCurrency(Currency currency);

    void deleteAllByCurrency(Currency currency);


    // select rate from history order by date desc limit 1
//    @Query("select rate from history order by date desc")
//    double findFirstByCurrencyOrderByDateDesc(Currency currency);
}
