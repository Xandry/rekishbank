package by.bsuir.rekishbank.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

// Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

// Persistence
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account")
    private Integer id;

    @Column(name = "open_date")
    @NotNull(message = "{account.openDate.notnull}")
    private Date openDate;

    @Column(name = "account_amount")
    @Min(value = 0, message = "{account.accountAmount.min}")
    private double accountAmount;

    @Column(name = "name")
    @Pattern(regexp = "[А-Яа-я0-9\\w,. \\-№]{1,45}",
            message = "{account.name.pattern}")
    private String name;

    @ManyToOne(optional = false,
            cascade = {CascadeType.DETACH, CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "client_id_client")
    @NotNull(message = "{account.client.notnull}")
    private Client client;


    @ManyToOne(optional = false,
            cascade = {CascadeType.DETACH, CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "currency_id_currency")
    @NotNull(message = "{account.currency.notnull}")
    private Currency currency;


    @ManyToOne(optional = true,
            cascade = {CascadeType.DETACH, CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "contribution_id_contribution")
    private Contribution contribution;


    @OneToOne(optional = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "card_card_number")
    private Card card;

    public Account(Date openDate, double accountAmount) {
        this.openDate = openDate;
        this.accountAmount = accountAmount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", openDate=" + openDate +
                ", accountAmount=" + accountAmount +
                ", name='" + name + '\'' +
                '}';
    }
}
