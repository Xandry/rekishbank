package by.bsuir.rekishbank.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

// Lombok
@Data
@NoArgsConstructor

// Persistence
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private int id;

    @Column(name = "username")
    @Pattern(regexp = "^[0-9\\w,. \\-№]{5,45}$", message = "{user.username.pattern}")
    private String username;

    @Column(name = "password")
    @Pattern(regexp = "^[0-9\\w,. \\-№{}]{5,60}$", message = "{user.passwordHash.pattern}")
    private String passwordHash;

    @ManyToOne(optional = false,
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "role_id_role")
    private Role role;

    @OneToOne(optional = true,
            mappedBy = "user",
            cascade = CascadeType.ALL)
    private Client client;

    public User(String username, String passwordHash) {
        this.username = username;
        this.passwordHash = passwordHash;
    }
}
