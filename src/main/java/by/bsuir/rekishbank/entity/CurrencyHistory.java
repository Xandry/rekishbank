package by.bsuir.rekishbank.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "history")
public class CurrencyHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "rate")
    @NotNull(message = "{history.rate.notnull}")
    @Positive(message = "{history.rate.positive}")
    private Double rate;

    @Column(name = "date")
    @NotNull(message = "{history.date.notnull}")
    private Date date;

    @ManyToOne(optional = false,
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "currency_id_currency")
    @NotNull(message = "{history.currency.notnull}")
    private Currency currency;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyHistory that = (CurrencyHistory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(rate, that.rate) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rate, date);
    }

    public CurrencyHistory(Double rate, Date date, Currency currency) {
        this.rate = rate;
        this.date = date;
        this.currency = currency;
    }
}
