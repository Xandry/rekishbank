package by.bsuir.rekishbank.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

// Lombok
@Data
@NoArgsConstructor

// Persistence
@Entity
@Table(name = "card")
public class Card {

    public static final int CARD_LIFE_IN_YEARS = 4;

    // TODO: 24.11.2019 Change to varchar(16)

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_number")
    private int cardNumber;

    @Column(name = "expiration_date")
    @NotNull(message = "{card.date.notnull}")
    private Date expirationDate;

    @Column(name = "cvc")
    @Min(value = 0, message = "{card.cvc.pattern}")
    @Max(value = 999, message = "{card.cvc.pattern}")
    private int cvc;

    @Column(name = "is_active")
    private boolean isActive = true;

    @OneToOne(optional = true,
            mappedBy = "card",
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Account account;

    public Card(Date expirationDate, int cvc) {
        this.expirationDate = expirationDate;
        this.cvc = cvc;
    }
}
