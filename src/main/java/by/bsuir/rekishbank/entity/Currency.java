package by.bsuir.rekishbank.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Objects;

// Lombok
@Data
@NoArgsConstructor

// Persistence
@Entity
@Table(name = "currency")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_currency")
    private int id;

    @Column(name = "name")
    @Pattern(regexp = "^[A-Z]{3}$",
            message = "{currency.name.pattern}")
    private String name;

    @OneToMany(mappedBy = "currency", cascade = CascadeType.ALL)
    private List<CurrencyHistory> currencyHistoryList;

    public Currency(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return id == currency.id &&
                Objects.equals(name, currency.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
