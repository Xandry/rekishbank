package by.bsuir.rekishbank.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

// Lombok
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

// Persistence
@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    private int id;

    @Column(name = "first_name")
    @NotNull(message = "{client.firstName.notnull}")
    @Pattern(regexp = "^[A-Za-z -]{1,45}$", message = "{client.firstName.pattern}")
    private String firstName;

    @Column(name = "last_name")
    @NotNull(message = "{client.lastName.notnull}")
    @Pattern(regexp = "^[A-Za-z -]{1,45}$", message = "{client.lastName.pattern}")
    private String lastName;

    @Column(name = "passport_number")
    @NotNull(message = "{client.passportNumber.notnull}")
    @Pattern(regexp = "^[A-Z0-9]{9}$", message = "{client.passportNumber.pattern}")
    private String passportNumber;

    @Column(name = "phone_number")
    @NotNull(message = "{client.phoneNumber.notnull}")
    @Pattern(regexp = "^\\+[0-9]{10,14}$", message = "{client.phoneNumber.pattern}")
    private String phoneNumber;

    @Column(name = "address")
    @NotNull(message = "client.address.notnull")
    @Pattern(regexp = "^[А-Яа-я0-9\\w,. \\-№]{5,200}$", message = "{client.address.pattern}")
    private String address;

    @OneToMany(mappedBy = "client",
            cascade = CascadeType.ALL)
    private List<Account> accountList;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id_user")
    private User user;

    public Client(String firstName, String lastName, String passportNumber, String phoneNumber, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passportNumber = passportNumber;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }


}
