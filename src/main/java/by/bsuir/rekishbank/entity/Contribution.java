package by.bsuir.rekishbank.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

// Lombok
@Data
@NoArgsConstructor

// Persistence
@Entity
@Table(name = "contribution")
public class Contribution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_contribution")
    private int id;

    @Column(name = "name")
    @NotBlank(message = "{contribution.name.notblank}")
    @Pattern(regexp = "[А-Яа-я0-9\\w,. \\-№]{1,45}",
            message = "{contribution.name.pattern}")
    private String name;

    @Column(name = "term")
    @Min(value = 1, message = "{contribution.term.min}")
    private int term;

    @Column(name = "interest_rate")
    @Min(value = 0, message = "{contribution.interestRate.min}")
    private double interestRate;

    @Column(name = "incoming_operation")
    private boolean incomeOperation;

    @Column(name = "expense_operation")
    private boolean expenseOperation;

    public Contribution(String name, int term, double interestRate, boolean incomeOperation, boolean expenseOperation) {
        this.name = name;
        this.term = term;
        this.interestRate = interestRate;
        this.incomeOperation = incomeOperation;
        this.expenseOperation = expenseOperation;
    }
}
