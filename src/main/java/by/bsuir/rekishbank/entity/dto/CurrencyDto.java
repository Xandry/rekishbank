package by.bsuir.rekishbank.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class CurrencyDto {

    private int id;

    @Pattern(regexp = "^[A-Z]{3}$",
            message = "{currency.name.pattern}")
    private String name;

}
