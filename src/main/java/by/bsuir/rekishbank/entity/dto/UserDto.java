package by.bsuir.rekishbank.entity.dto;

import by.bsuir.rekishbank.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

// lombok
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class UserDto {

    private int id;

    @NotBlank
    @Pattern(regexp = "^[0-9\\w,. \\-№]{5,45}$", message = "{user.username.pattern}")
    private String username;

    @NotBlank
    @Pattern(regexp = "^[0-9\\w,. \\-№{}]{5,60}$", message = "{user.passwordHash.pattern}")
    private String passwordHash;

    private Role role;

}
