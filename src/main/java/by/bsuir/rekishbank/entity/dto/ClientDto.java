package by.bsuir.rekishbank.entity.dto;

import by.bsuir.rekishbank.entity.Client;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class ClientDto {

    private Integer id;

    @NotNull(message = "{client.firstName.notnull}")
    @Pattern(regexp = "^[A-Za-z -]{1,45}$", message = "{client.firstName.pattern}")
    private String firstName;

    @NotNull(message = "{client.lastName.notnull}")
    @Pattern(regexp = "^[A-Za-z -]{1,45}$", message = "{client.lastName.pattern}")
    private String lastName;

    @NotNull(message = "{client.passportNumber.notnull}")
    @Pattern(regexp = "^[A-Z0-9]{9}$", message = "{client.passportNumber.pattern}")
    private String passportNumber;

    @NotNull(message = "{client.phoneNumber.notnull}")
    @Pattern(regexp = "^\\+[0-9]{10,14}$", message = "{client.phoneNumber.pattern}")
    private String phoneNumber;

    @NotNull(message = "client.address.notnull")
    @Pattern(regexp = "^[А-Яа-я0-9\\w,. \\-№]{5,200}$", message = "{client.address.pattern}")
    private String address;

    public ClientDto(Client client) {
        this.id = client.getId();
        this.firstName = client.getFirstName();
        this.lastName = client.getLastName();
        this.passportNumber = client.getPassportNumber();
        this.phoneNumber = client.getPhoneNumber();
        this.address = client.getAddress();
    }
}
