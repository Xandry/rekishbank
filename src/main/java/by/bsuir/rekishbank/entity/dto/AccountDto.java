package by.bsuir.rekishbank.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class AccountDto {

    private Integer id;

    @NotNull(message = "{account.openDate.notnull}")
    private Date openDate;

    @Min(value = 0, message = "{account.accountAmount.min}")
    private double accountAmount;

    @Pattern(regexp = "[А-Яа-я0-9\\w,. \\-№]{1,45}",
            message = "{account.name.pattern}")
    private String name;

}
