package by.bsuir.rekishbank;

public class Pages {

    public static final String ERROR_404_NOT_FOUND = "error/error-404";
    public static final String ERROR_403_FORBIDDEN = "error/error-403";
    public static final String ERROR = "error/error";

    public static final String CLIENT_LIST = "admin/client/clients";
    public static final String CLIENT_ADD = "admin/client/client-add";
    public static final String CLIENT_GET = "admin/client/client";
    public static final String CLIENT_UPDATE = "admin/client/client-update";

    public static final String REGISTRATION = "registration";
    public static final String REGISTRATION_SUCCESS = "successRegister";

    public static final String CABINET = "cabinet";
    public static final String CABINET_ACCOUNT = "cabinet-account-id";
    public static final String CABINET_ACCOUNT_SET_NAME = "cabinet-account-setname";

    public static final String ACCOUNTS_LIST = "admin/account/accounts";
    public static final String ACCOUNT_PAGE = "admin/account/account";
    public static final String ACCOUNT_ADD = "admin/account/account-add";
    public static final String ACCOUNT_UPDATE = "admin/account/account-update";

    public static final String USERS_LIST = "admin/user/users";
    public static final String USER_GET = "admin/user/user";
    public static final String USER_ADD = "admin/user/user-add";

    public static final String CURRENCY_LIST = "currencies";
    public static final String CURRENCY_GET = "currency";
    public static final String CURRENCY_ADD = "admin/currency/currency-add";

    public static final String CONTRIBUTION_LIST = "contributions";
    public static final String CONTRIBUTION_GET = "contribution";

    private Pages() {
    }

}
