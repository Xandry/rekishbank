package by.bsuir.rekishbank.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Objects;

@Configuration
@PropertySource("classpath:application.properties")
public class DataSourceConfig {

    private final Environment environment;

    public DataSourceConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public DataSource securityDataSource() {
        ComboPooledDataSource securityDataSource
                                = new ComboPooledDataSource();

        try {
            securityDataSource.setDriverClass(
                    environment.getProperty("spring.datasource.driver-class-name"));
        } catch (PropertyVetoException e) {

            throw new RuntimeException(e);
        }
        securityDataSource.setJdbcUrl(environment.getProperty("spring.datasource.url"));
        securityDataSource.setPassword(environment.getProperty("spring.datasource.password"));
        securityDataSource.setUser(environment.getProperty("spring.datasource.username"));

        securityDataSource.setInitialPoolSize(Integer.parseInt(Objects.requireNonNull(
                environment.getProperty("c3p0.initial-pool-size"))));
        securityDataSource.setMaxPoolSize(Integer.parseInt(Objects.requireNonNull(
                environment.getProperty("c3p0.max-pool-size"))));
        securityDataSource.setMinPoolSize(Integer.parseInt(Objects.requireNonNull(
                environment.getProperty("c3p0.min-pool-size"))));
        securityDataSource.setMaxIdleTime(Integer.parseInt(Objects.requireNonNull(
                environment.getProperty("c3p0.max-idle-time"))));

        return securityDataSource;
    }


}
