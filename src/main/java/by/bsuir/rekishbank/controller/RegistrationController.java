package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.dto.UserDto;
import by.bsuir.rekishbank.service.RoleService;
import by.bsuir.rekishbank.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class RegistrationController {

    private final UserService userService;

    private final RoleService roleService;

    public RegistrationController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @GetMapping("/registration")
    public ModelAndView showRegistrationForm() {
        UserDto userDto = new UserDto();
        return new ModelAndView(Pages.REGISTRATION, "user", userDto);
    }

    @GetMapping("/registration/success")
    public String showRegistrationSuccessPage() {
        return Pages.REGISTRATION_SUCCESS;
    }

    @PostMapping("/registration")
    public String registerUser(@Valid @ModelAttribute("user") UserDto userDto,
                               BindingResult bindingResult,
                               Model model) {
        if (!bindingResult.hasErrors()) {
            if (!userService.getByUsername(userDto.getUsername()).isPresent()) {
                userDto.setRole(roleService.getByName("ROLE_USER"));
                userService.addUser(userDto);
                model.addAttribute("user", userDto);
                return "redirect:/" + "registration/success";
            } else {
                model.addAttribute("error", "User with such login already exists");
                return Pages.ERROR;
            }
        } else {
            return Pages.REGISTRATION;
        }
    }
}
