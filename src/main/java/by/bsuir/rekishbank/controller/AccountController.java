package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.*;
import by.bsuir.rekishbank.service.*;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/accounts")

public class AccountController {
    private static final String REDIRECT = "redirect:/";
    private static final String ACCOUNTS = "accounts";
    private static final String ACCOUNT = "account";

    private final AccountService accountService;

    private final UserService userService;

    private final ContributionService contributionService;

    private final ClientService clientService;

    private final CurrencyService currencyService;

    private final CardService cardService;

    public AccountController(AccountService accountService,
                             UserService userService,
                             ContributionService contributionService,
                             ClientService clientService,
                             CurrencyService currencyService,
                             CardService cardService) {
        this.accountService = accountService;
        this.userService = userService;
        this.contributionService = contributionService;
        this.clientService = clientService;
        this.currencyService = currencyService;
        this.cardService = cardService;
    }

    /**
     * Casts date &lt;form:input type="date" ... &gt; to java-like date.
     *
     * @param binder binder that process this casting.
     */
    @InitBinder
    private void dateBinder(WebDataBinder binder) {
        //The date format to parse or output your dates
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);
    }

    ////////////////////////////////////////////////////////////////////////

    @GetMapping
    public String getAccounts(Model model) {
        model.addAttribute(ACCOUNTS, accountService.getAllAccounts());
        model.addAttribute(ACCOUNT, new Account());

        List<Contribution> contributionList = new ArrayList<>(contributionService.getAllContributions());
        model.addAttribute("contributions", contributionList);

        List<User> userList = new ArrayList<>(userService.getAllUsers());
        model.addAttribute("users", userList);

        List<Client> clientList = new ArrayList<>(clientService.getAllClients());
        model.addAttribute("clients", clientList);

        List<Currency> currencyList = new ArrayList<>(currencyService.getAllCurrencies());
        model.addAttribute("currencies", currencyList);

        return Pages.ACCOUNTS_LIST;
    }

    @GetMapping("/{accountId}")
    public String getAccount(@PathVariable("accountId") Integer accountId,
                             Model model) {
        Optional<Account> account = accountService.getById(accountId);
        if (account.isPresent()) {
            model.addAttribute(ACCOUNT, account.get());
            if (account.get().getCard() != null) {
                model.addAttribute("expdate",
                        cardService.formatDate(account.get().getCard().getExpirationDate()));
            }
            return Pages.ACCOUNT_PAGE;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    ////////////////////////////////////////////////////////////////////////

    @GetMapping("/add")
    public String showAddAccountPage(Model model) {
        model.addAttribute("account", new Account());
        List<Contribution> contributionList = new ArrayList<>(contributionService.getAllContributions());
        model.addAttribute("contributions", contributionList);

        List<Client> clientList = new ArrayList<>(clientService.getAllClients());
        model.addAttribute("clients", clientList);

        List<Currency> currencyList = new ArrayList<>(currencyService.getAllCurrencies());
        model.addAttribute("currencies", currencyList);

        return Pages.ACCOUNT_ADD;
    }

    @PostMapping("/add")
    public String addAccount(@Valid @ModelAttribute("account") Account account,
//                             @RequestParam
                                     BindingResult bindingResult,
                             Model model) {
        if (!bindingResult.hasErrors()) {
            accountService.addAccount(account);

            return REDIRECT + "admin/accounts/";
        } else {
            return showAddAccountPage(model);
        }
    }

    ////////////////////////////////////////////////////////////////////////

    @GetMapping("/delete")
    public String deleteAccount(@RequestParam("id") Integer accountId,
                                Model model) {
        if (accountService.isExist(accountId)) {
            accountService.deleteAccount(accountId);
            return REDIRECT + "admin/accounts";
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    ////////////////////////////////////////////////////////////////////////

    @PostMapping("/{accountId}/changeCardStatus")
    public String changeCardStatus(@PathVariable("accountId") Integer accountId,
                                   Model model) {
        Optional<Account> accountOptional = accountService.getById(accountId);
        if (accountOptional.isPresent()) {
            Card userCard = accountOptional.get().getCard();
            cardService.changeCardStatus(userCard);
            return REDIRECT + "admin/accounts/" + accountId;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }
}