package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.Contribution;
import by.bsuir.rekishbank.service.ContributionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ContributionController {

    private static final String CONTRIBUTION = "contribution";
    private static final String CONTRIBUTIONS = "contributions";

    private final ContributionService contributionService;

    public ContributionController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    @GetMapping("/contributions")
    public String getContributions(Model model) {
        model.addAttribute(CONTRIBUTIONS, contributionService.getAllContributions());
        model.addAttribute(CONTRIBUTION, new Contribution());
        return Pages.CONTRIBUTION_LIST;
    }

    @PostMapping("/admin/contributions")
    public String addContribution(Model model,
                                  @Valid @ModelAttribute(CONTRIBUTION) Contribution contribution,
                                  BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            contributionService.addContribution(contribution);
            return "redirect:/contributions";
        } else {
            model.addAttribute(CONTRIBUTIONS, contributionService.getAllContributions());
            return Pages.CONTRIBUTION_LIST;
        }
    }

    @GetMapping("contributions/{" + CONTRIBUTION + "Id}")
    public String getContribution(@PathVariable(CONTRIBUTION + "Id") Integer id,
                                  Model model) {
        Optional<Contribution> contributionOptional = contributionService.getById(id);
        if (contributionOptional.isPresent()) {
            model.addAttribute(CONTRIBUTION, contributionOptional.get());
            return Pages.CONTRIBUTION_GET;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    @PostMapping("admin/contributions/{" + CONTRIBUTION + "Id}")
    public String updateContribution(@PathVariable(CONTRIBUTION + "Id") Integer contributionId,
                                     @Valid @ModelAttribute(CONTRIBUTION) Contribution updatedContribution,
                                     BindingResult bindingResult,
                                     Model model) {
        if (contributionService.isExists(contributionId)) {
            if (!bindingResult.hasErrors()) {
                contributionService.updateContribution(updatedContribution, contributionId);
                return "redirect:/contributions";
            } else {
                model.addAttribute(CONTRIBUTION, contributionService.getById(contributionId).get());
                return Pages.CONTRIBUTION_LIST;
            }
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    @GetMapping("admin/contributions/delete")
    public String deleteContribution(@RequestParam(name = "id") Integer contributionId,
                                     Model model) {
        if (contributionService.isExists(contributionId)) {
            contributionService.deleteContribution(contributionId);
            return "redirect:/contributions";
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

}
