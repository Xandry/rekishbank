package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.Client;
import by.bsuir.rekishbank.entity.dto.ClientDto;
import by.bsuir.rekishbank.service.CardService;
import by.bsuir.rekishbank.service.ClientService;
import by.bsuir.rekishbank.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/admin/clients")
public class ClientController {

    private static final String CLIENT = "client";
    private static final String CLIENTS = "clients";
    private final ClientService clientService;
    private final UserService userService;


    public ClientController(ClientService clientService, UserService userService, CardService cardService) {
        this.clientService = clientService;
        this.userService = userService;
    }

    @GetMapping
    public String getClients(Model model) {
        model.addAttribute(CLIENTS, clientService.getAllClients());
        return Pages.CLIENT_LIST;
    }

    @GetMapping("/{clientId}")
    public String getClient(@PathVariable("clientId") Integer clientId, Model model) {
        model.addAttribute("client", clientService.getById(clientId).orElse(null));
        return Pages.CLIENT_GET;
    }

    ////////////////////////////////////////////////////////

    @GetMapping("/add")
    public String getAddClientPage(Model model) {
        model.addAttribute(CLIENT, new Client());

        model.addAttribute("users", userService.getUserUsernameMap());
        return Pages.CLIENT_ADD;
    }

    @PostMapping("/add")
    public String addClient(@Valid @ModelAttribute(CLIENT) Client client,
                            BindingResult bindingResult,
                            Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("users", userService.getUserUsernameMap());
            return Pages.CLIENT_ADD;
        }
        clientService.addClient(client);
        return "redirect:/admin/clients";
    }

    ////////////////////////////////////////////////////////

    @GetMapping("/{clientId}/update")
    public String showClientUpdatePage(@PathVariable("clientId") Integer clientId,
                                       Model model) {
        Optional<ClientDto> clientDto = clientService.getDtoById(clientId);
        if (clientDto.isPresent()) {
            model.addAttribute("clientDto", clientDto.get());
            return Pages.CLIENT_UPDATE;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    @PostMapping("/{clientId}/update")
    public String updateClient(@PathVariable("clientId") Integer clientId,
                               @Valid @ModelAttribute("clientDto") ClientDto clientDto,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Pages.CLIENT_UPDATE;
        }
        clientService.updateClient(clientDto, clientId);
        return "redirect:/admin/clients/" + clientId;
    }

    ////////////////////////////////////////////////////////


    @GetMapping("/delete")
    public String deleteClient(@RequestParam("id") Integer clientId,
                               Model model) {
        if (clientService.isClientExists(clientId)) {
            clientService.deleteClient(clientId);
            return getClients(model);
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }
}
