package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.Currency;
import by.bsuir.rekishbank.entity.CurrencyHistory;
import by.bsuir.rekishbank.entity.dto.CurrencyDto;
import by.bsuir.rekishbank.service.CurrencyHistoryService;
import by.bsuir.rekishbank.service.CurrencyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.*;

@Controller
public class CurrencyController {

    private final CurrencyService currencyService;

    private final CurrencyHistoryService currencyHistoryService;

    public CurrencyController(CurrencyService currencyService, CurrencyHistoryService currencyHistoryService) {
        this.currencyService = currencyService;
        this.currencyHistoryService = currencyHistoryService;
    }

    ///////////////////////////////////////////////////////////////////////////////////

    @GetMapping("/currencies")
    public String getCurrencies(Model model) {
        List<Currency> currencies = currencyService.getAllCurrencies();
        model.addAttribute("currencies", currencies);
        model.addAttribute("currency", new Currency());
        model.addAttribute("currencyDto", new CurrencyDto());
        Map<Currency, Double> currencyRatesMap = new HashMap<>();
        for (Currency x : currencies) {
            currencyRatesMap.put(x, currencyService.getRate(x));
        }
        model.addAttribute("rates", currencyRatesMap);
        return Pages.CURRENCY_LIST;
    }

    @GetMapping("/currencies/{currencyId}")
    public String getCurrency(@PathVariable(name = "currencyId") Integer currencyId, Model model) {

        Optional<Currency> currencyOptional = currencyService.getById(currencyId);
        if (currencyOptional.isPresent()) {
            Currency currency = currencyOptional.get();

            List<CurrencyHistory> currencyHistoryList = currencyHistoryService.getAllHistory(currency);
            currencyHistoryList.sort(Comparator.comparing(CurrencyHistory::getDate));
            model.addAttribute("currency", currency);
            model.addAttribute("rates", currencyHistoryList);
            model.addAttribute("currentRate", currencyService.getRate(currency));
            return Pages.CURRENCY_GET;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

    @GetMapping("/admin/currencies/add")
    public String showCurrencyAddPage(Model model) {
        model.addAttribute("currency", new Currency());
        return Pages.CURRENCY_ADD;
    }

    @PostMapping("/admin/currencies/add")
    public String addCurrency(@Valid @ModelAttribute("currency") Currency currency,
                              BindingResult bindingResult,
                              @Valid @Positive @RequestParam("rate") Double rate,
                              Model model) {
        if (!bindingResult.hasErrors()) {
            currencyService.addCurrency(currency);
            CurrencyHistory currencyHistory = new CurrencyHistory(rate, new Date(), currency);
            currencyHistoryService.addCurrencyHistory(currencyHistory);
            return "redirect:/currencies";
        } else {
            return Pages.CURRENCY_ADD;
        }
    }

    /////////////////////////////////////////////////////////////////////

    @PostMapping("/admin/currencies/{currencyId}/addRate")
    public String addCurrencyRate(@PathVariable("currencyId") Integer currencyId,
                                  @Valid @Positive @RequestParam("value") Double value) {
        Optional<Currency> currencyOptional = currencyService.getById(currencyId);
        if (currencyOptional.isPresent()) {
            CurrencyHistory currencyHistory = new CurrencyHistory(value, new Date(), currencyOptional.get());
            currencyHistoryService.addCurrencyHistory(currencyHistory);
            return "redirect:/currencies/" + currencyId;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    ////////////////////////////////////////////////////////////////////////

    @PostMapping("/admin/currencies/{currencyId}")
    public String updateCurrency(@PathVariable(name = "currencyId") Integer currencyId,
                                 @ModelAttribute("currency") Currency updatedCurrency,
                                 Model model) {
        if (currencyService.getById(currencyId).isPresent()) {
            currencyService.updateCurrency(updatedCurrency, currencyId);
            return "redirect:/admin/currencies/" + currencyId;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }


    @GetMapping("/admin/currencies/delete")
    public String deleteCurrency(@RequestParam(name = "id") Integer currencyId,
                                 Model model) {
        if (currencyService.isExist(currencyId)) {
            currencyService.deleteCurrency(currencyId);
            return "redirect:/currencies";
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }
}
