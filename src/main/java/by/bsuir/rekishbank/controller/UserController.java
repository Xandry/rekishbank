package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.User;
import by.bsuir.rekishbank.service.RoleService;
import by.bsuir.rekishbank.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("admin/users")
public class UserController {

    private final UserService userService;

    private final RoleService roleService;

    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @GetMapping
    public String getUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("user", new User());
        return Pages.USERS_LIST;
    }

    @GetMapping("/{userId}")
    public String getUser(@PathVariable("userId") Integer userId, Model model) {
        Optional<User> userOptional = userService.getById(userId);
        if (userOptional.isPresent()) {
            model.addAttribute("user", userOptional.get());
            return Pages.USER_GET;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    @GetMapping("/add")
    public ModelAndView showAddUserPage() {
        ModelAndView modelAndView = new ModelAndView(Pages.USER_ADD, "user", new User());
        modelAndView.addObject("roles", roleService.getRoleStringMap());
        return modelAndView;
    }

    @PostMapping("/add")
    public String addUser(@Valid @ModelAttribute("user") User user,
                          BindingResult bindingResult,
                          Model model) {
        if (!bindingResult.hasErrors()) {
            if (!userService.getByUsername(user.getUsername()).isPresent()) {
                userService.addUser(user);
                return "redirect:/admin/users";
            } else {
                model.addAttribute("error", "User with username is already exists");
                return Pages.ERROR;

            }
        } else {
            model.addAttribute("roles", roleService.getRoleStringMap());
            return Pages.USER_ADD;
        }
    }

    @PostMapping("{userId}")
    public String updateUser(@PathVariable("userId") Integer userId,
                             @ModelAttribute("user") User user,
                             Model model) {
        if (userService.isExists(userId)) {
            userService.updateUser(user, userId);
            model.addAttribute("user", user);
            return "redirect:/admin/users/" + userId;
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam(name = "id") Integer userId,
                             Model model) {
        if (userService.isExists(userId)) {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Optional<User> user = userService.getByUsername(auth.getName());
            if (isItMe(userId)) {
                model.addAttribute("error", "You cannot delete yourself");
                return Pages.ERROR;
            }
            userService.deleteUser(userId);
            return "redirect:/admin/users";
        } else {
            return Pages.ERROR_404_NOT_FOUND;
        }
    }

    /**
     * @param userId user id.
     * @return is this user are authorised user.
     */
    private boolean isItMe(Integer userId) {
        if (userService.isExists(userId)) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Optional<User> userAuthorized = userService.getByUsername(auth.getName());
            return userAuthorized.filter(user -> userId.equals(user.getId())).isPresent();

        } else {
            return false;
        }
    }
}