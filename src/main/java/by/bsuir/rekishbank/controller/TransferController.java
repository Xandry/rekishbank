package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.service.AccountService;
import by.bsuir.rekishbank.service.CardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.NoSuchElementException;

@Controller
@RequestMapping("/transfer")
@Slf4j
public class TransferController {

    private final CardService cardService;

    public TransferController(CardService cardService, AccountService accountService) {
        this.cardService = cardService;
    }

    @GetMapping
    public String showTransferPage() {
        return "transfer";
    }

    @PostMapping
    public String handleTransfer(@RequestParam("card_number_from") Integer cardNumberFrom,
                                 @RequestParam("expdate") String expDate,
                                 @RequestParam("cvc") Integer cvc,
                                 @RequestParam("cardholder") String cardHolder,
                                 @RequestParam("amount") Double amount,
                                 @RequestParam("card_number_to") Integer cardNumberTo) {

        log.trace("Starting card to card transaction");
        try {
            if (!cardService.isValid(cardNumberFrom, expDate, cvc, cardHolder)) {
                log.trace("Card to card transaction failed - wrong credentials");
                throw new NoSuchElementException("Wrong \"from\" card credentials");
            }
            if (!cardService.isValid(cardNumberTo)) {
                log.trace("Card to card transaction failed - wrong credentials");
                throw new NoSuchElementException("Wrong \"to\" card credentials");
            }
            cardService.cardToCardTransfer(cardNumberFrom, cardNumberTo, amount);
            log.trace("Successful card to card transaction");
            return "redirect:" + showTransferPage();
        } catch (Exception e) {
            return "redirect:/transfer/error";
        }
    }

    @GetMapping("/error")
    public String showTransferError() {
        return "transfer-error";
    }


}
