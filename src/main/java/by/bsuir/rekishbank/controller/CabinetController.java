package by.bsuir.rekishbank.controller;

import by.bsuir.rekishbank.Pages;
import by.bsuir.rekishbank.entity.*;
import by.bsuir.rekishbank.service.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cabinet")
@Slf4j
public class CabinetController {

    private static final String REDIRECT = "redirect:/";

    private final CardService cardService;

    private final UserService userService;

    private final ClientService clientService;

    private final CurrencyService currencyService;

    private final ContributionService contributionService;

    private final AccountService accountService;

    public CabinetController(CardService cardService,
                             UserService userService,
                             ClientService clientService,
                             CurrencyService currencyService,
                             ContributionService contributionService,
                             AccountService accountService) {
        this.cardService = cardService;
        this.userService = userService;
        this.clientService = clientService;
        this.currencyService = currencyService;
        this.contributionService = contributionService;
        this.accountService = accountService;
    }

    /**
     * Casts date &lt;form:input type="date" ... &gt; to java-like date.
     *
     * @param binder binder that process this casting.
     */
    @InitBinder
    private void dateBinder(WebDataBinder binder) {
        //The date format to parse or output your dates
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);
    }

    @GetMapping
    public String showCabinet(Model model) {
        Optional<User> user = getAuthenticatedUser();
        if (user.isPresent()) {
            model.addAttribute("me", user.get());

            if (user.get().getClient() == null) {
                model.addAttribute("client", new Client());
            } else {
                model.addAttribute("account", new Account());

                List<Currency> currencyList = new ArrayList<>(currencyService.getAllCurrencies());
                model.addAttribute("currencies", currencyList);

                List<Contribution> contributionList = new ArrayList<>(contributionService.getAllContributions());
                model.addAttribute("contributions", contributionList);
            }
            return Pages.CABINET;
        } else {
            log.info("Attempt of an unauthorized access to cabinet");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    @PostMapping("/create-client")
    public String createClientForUser(@Valid @ModelAttribute("client") Client client,
                                      BindingResult bindingResult,
                                      Model model) {
        Optional<User> user = getAuthenticatedUser();
        if (user.isPresent()) {
            if (!bindingResult.hasErrors()) {

                if (user.get().getClient() == null) {
                    user.get().setClient(client);
                    client.setUser(user.get());
                    clientService.addClient(client);
                }
                return REDIRECT + "cabinet";

            } else {
                model.addAttribute("me", user.get());
                return Pages.CABINET;
            }
        } else {
            log.info("Attempt of an unauthorized access to cabinet");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    @PostMapping("/accounts")
    public String addMyAccount(@ModelAttribute("account") Account account,
                               Model model) {

        Optional<User> userOptional = getAuthenticatedUser();
        userOptional.ifPresent(user -> {
            account.setClient(user.getClient());
            user.getClient().getAccountList().add(account);
            account.setOpenDate(new Date());
            account.setAccountAmount(0);
            accountService.addAccount(account);
        });
        return REDIRECT + "cabinet";
    }

    @GetMapping("/accounts/{accountId}")
    public String showMyAccount(@PathVariable("accountId") Integer accountId,
                                Model model) {
        Optional<Account> accountOptional = getAccountOfAuthenticatedUserById(accountId);
        if (accountOptional.isPresent()) {
            model.addAttribute("account", accountOptional.get());
            if (accountOptional.get().getCard() != null) {
                String expDate = cardService.formatDate(accountOptional.get().getCard().getExpirationDate());
                model.addAttribute("expdate", expDate);
            }
            return Pages.CABINET_ACCOUNT;
        } else {
            log.info("Attempt of an unauthorized access to cabinet");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    /////////////////////////////////////////////////////////////////////////


    @PostMapping("/accounts/{accountId}/renewalApplication")
    public String renewalApplication(@PathVariable("accountId") Integer accountId) {
        Optional<Account> account = getAccountOfAuthenticatedUserById(accountId);
        if (account.isPresent() && account.get().getCard() != null) {
            cardService.createExpirationDate(account.get().getCard());
        }
        return REDIRECT + "cabinet/accounts/" + accountId;

    }

    @PostMapping("/accounts/{accountId}/createCard")
    public String createCardForMyAccount(@PathVariable("accountId") Integer accountId,
                                         Model model) {


        Optional<Account> account = getAccountOfAuthenticatedUserById(accountId);
        if (account.isPresent()) {
            cardService.generateCardForAccount(account.get());
            model.addAttribute("me", account.get());
            return REDIRECT + "cabinet/accounts/" + accountId;
        } else {
            log.info("Attempt of an unauthorized access to cabinet");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    @GetMapping("accounts/{accountId}/setName")
    public String showSetNamePage(@PathVariable("accountId") Integer accountId,
                                  Model model) {
        Optional<User> me = getAuthenticatedUser();
        if (me.isPresent()) {
            Optional<Account> accountOfAuthenticatedUser = getAccountOfAuthenticatedUserById(accountId);
            if (accountOfAuthenticatedUser.isPresent()) {
                model.addAttribute("account", new Account());
                model.addAttribute("accountId", accountId);
                return Pages.CABINET_ACCOUNT_SET_NAME;
            } else {
                log.info("Attempt of an unauthorized access to other's person account #" + accountId);
                return Pages.ERROR_403_FORBIDDEN;
            }
        } else {
            log.info("Attempt of an unauthorized access of an unauthenticated user");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    @PostMapping("accounts/{accountId}/setName")
    public String setName(@PathVariable("accountId") Integer accountId,
                          @Valid @Length(min = 1, max = 45) @RequestParam("name") String name,
                          Model model) {

        Optional<Account> userAccount = getAccountOfAuthenticatedUserById(accountId);
        if (!userAccount.isPresent()) {
            return Pages.ERROR_403_FORBIDDEN;
        }

        accountService.setAccountName(name, userAccount.get().getId());
        model.addAttribute("me", userAccount.get());
        return REDIRECT + "cabinet/accounts/" + accountId;

    }

    //////////////////////////////////////////////////////////////////////////////////////

    @PostMapping("accounts/{accountId}/changeCardStatus")
    public String changeCartStatus(@PathVariable("accountId") Integer accountId) {

        Optional<Account> userAccount = getAccountOfAuthenticatedUserById(accountId);
        if (userAccount.isPresent()) {
            Card userCard = userAccount.get().getCard();
            cardService.changeCardStatus(userCard);
            return REDIRECT + "cabinet/accounts/" + accountId;
        } else {
            log.info("Attempt of an unauthorized access to changing card status");
            return Pages.ERROR_403_FORBIDDEN;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check if user has an account with given id.
     *
     * @param accountId id of the occaunt to check.
     * @return optional value.
     */
    private Optional<Account> getAccountOfAuthenticatedUserById(Integer accountId) {
        Optional<User> userOptional = getAuthenticatedUser();

        final Account[] usersAccount = {null};
        userOptional.ifPresent(user -> {
            for (Account account : user.getClient().getAccountList()) {
                if (account.getId().equals(accountId)) {
                    usersAccount[0] = account;
                    break;
                }
            }
        });
        return Optional.ofNullable(usersAccount[0]);
    }

    private Optional<User> getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.getByUsername(auth.getName());
    }
}
