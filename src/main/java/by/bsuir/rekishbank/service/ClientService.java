package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Client;
import by.bsuir.rekishbank.entity.dto.ClientDto;
import by.bsuir.rekishbank.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Slf4j
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public boolean isClientExists(Integer id) {
        return getById(id).isPresent();
    }

    public List<Client> getAllClients() {
        log.trace("Getting add clients");
        ArrayList<Client> clientArrayList = new ArrayList<>();
        clientRepository.findAll().forEach(clientArrayList::add);
        return clientArrayList;
    }

    public void addClient(Client client) {
        clientRepository.save(client);
    }


    public void updateClient(Client client, Integer clientId) {
        log.trace("Updating client #" + clientId);
        client.setId(clientId);
        clientRepository.save(client);
    }

    public void updateClient(ClientDto clientDto, Integer clientId) {
        log.trace("Updating client #" + clientId);
        Client client = getById(clientId).orElseThrow(NoSuchElementException::new);
        client.setAddress(clientDto.getAddress());
        client.setFirstName(clientDto.getFirstName());
        client.setPhoneNumber(clientDto.getPhoneNumber());
        client.setLastName(clientDto.getLastName());
        client.setPassportNumber(clientDto.getPassportNumber());
        updateClient(client, clientId);
    }

    public void deleteClient(Client client) {
        log.trace("Deleting client #" + client.getId());
        clientRepository.delete(client);
    }

    public void deleteClient(int id) {
        log.trace("Deleting client #" + id);
        clientRepository.deleteById(id);
    }

    public Optional<Client> getById(Integer id) {
        return clientRepository.findById(id);
    }

    public Optional<ClientDto> getDtoById(Integer id) {
        Client client = getById(id).orElse(null);
        if (client != null) {
            ClientDto clientDto = new ClientDto(client);
            return Optional.of(clientDto);
        } else {
            return Optional.empty();
        }
    }

}
