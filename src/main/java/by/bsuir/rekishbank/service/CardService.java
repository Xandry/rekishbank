package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Account;
import by.bsuir.rekishbank.entity.Card;
import by.bsuir.rekishbank.entity.Client;
import by.bsuir.rekishbank.exception.CardIsBlockedException;
import by.bsuir.rekishbank.exception.InsufficientFundsException;
import by.bsuir.rekishbank.repository.CardRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class CardService {

    private final CardRepository cardRepository;

    private final AccountService accountService;

    private final Random random = new Random();

    public CardService(CardRepository cardRepository, AccountService accountService) {
        this.cardRepository = cardRepository;
        this.accountService = accountService;
    }

    public String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yy");
        return dateFormat.format(date);
    }

    /**
     * @param cardNumberFrom send money from this card resp. account
     * @param cardNumberTo   send money to this card resp. account
     * @param amount         amount of money
     * @throws NoSuchElementException     if card's credentials invalid
     * @throws InsufficientFundsException if sender account's amount of money is not enough
     */
    public void cardToCardTransfer(Integer cardNumberFrom, Integer cardNumberTo, Double amount)
            throws NoSuchElementException, InsufficientFundsException, CardIsBlockedException {
        log.trace("Starting card to card transaction");
        Card cardFrom = getByNumber(cardNumberFrom).orElseThrow(NoSuchElementException::new);
        Card cardTo = getByNumber(cardNumberTo).orElseThrow(NoSuchElementException::new);
        Account accountFrom = cardFrom.getAccount();
        Account accountTo = cardTo.getAccount();
        if (accountFrom.getAccountAmount() < amount) {
            InsufficientFundsException insufficientFundsException = new InsufficientFundsException();
            log.debug("Card to card transaction failed", insufficientFundsException);
            throw insufficientFundsException;
        }
        if (!cardFrom.isActive()) {
            CardIsBlockedException cardIsBlockedException = new CardIsBlockedException();
            log.debug("Card to card transaction failed", cardIsBlockedException);
            throw cardIsBlockedException;
        }
        log.debug("Successful card to card transaction");
        accountService.changeAmount(accountFrom, -1 * amount);
        accountService.changeAmount(accountTo, amount);
    }

    public boolean isValid(Integer cardNumberFrom, String expDate,
                           Integer cvc, String cardholderName) {
        try {
            Card card = getByNumberAndCvc(cardNumberFrom, cvc).orElseThrow(NoSuchElementException::new);
            return checkDate(card, expDate) && checkCardholderName(card, cardholderName);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isValid(Integer cardNumber) {

        return getByNumber(cardNumber).isPresent();

    }

    private boolean checkDate(Card card, String expDate) {
        String formatExpDate = formatDate(card.getExpirationDate());
        return formatExpDate.equals(expDate);
    }

    private boolean checkCardholderName(Card card, String cardHolderFromRequest) {
        Client cardholder = card.getAccount().getClient();
        String trueCardholderName = cardholder.getFirstName() + " " + cardholder.getLastName();
        return trueCardholderName.equalsIgnoreCase(cardHolderFromRequest);
    }

    public List<Card> getAllCards() {
        ArrayList<Card> currencyArrayList = new ArrayList<>();
        cardRepository.findAll().forEach(currencyArrayList::add);
        return currencyArrayList;
    }

    public Optional<Card> getByNumber(Integer number) {
        log.trace("Getting card by its number");
        return cardRepository.getByCardNumber(number);
    }

    public Optional<Card> getByNumberAndCvc(Integer number, Integer cvc) {
        return cardRepository.getByCardNumberAndCvc(number, cvc);
    }

    public void addCard(Card card) {
        log.trace("Saving card");
        cardRepository.save(card);
    }


    public void updateCard(Card card, Integer cardNumber) {
        log.trace("Updating card");
        card.setCardNumber(cardNumber);
        cardRepository.save(card);
    }

    public void updateCard(Card card) {
        log.trace("Updating card");
        cardRepository.save(card);
    }

    public void changeCardStatus(Card card) {
        card.setActive(!card.isActive());
        updateCard(card, card.getCardNumber());
    }

    public void createExpirationDate(Card card) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, Card.CARD_LIFE_IN_YEARS);
        Date newExpDate = cal.getTime();
        card.setExpirationDate(newExpDate);
        updateCard(card);
    }

    public void generateCardForAccount(Account account) {
        // TODO: 03.12.2019 ADD GENERATED CARD NUMBER
        log.trace("Generating a card for an account");
        Card card = new Card();
        createExpirationDate(card);
        card.setCvc(generateCvc());
        card.setAccount(account);
        account.setCard(card);
        addCard(card);
    }

    public void deleteCard(Card card) {
        cardRepository.delete(card);
    }

    public void deleteCard(int id) {
        cardRepository.deleteById(id);
    }

    public int generateCvc() {
        return random.nextInt(1000);
    }

}
