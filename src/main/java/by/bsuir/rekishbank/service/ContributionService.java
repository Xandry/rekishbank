package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Contribution;
import by.bsuir.rekishbank.repository.ContributionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ContributionService {

    private final ContributionRepository contributionRepository;

    public ContributionService(ContributionRepository contributionRepository) {
        this.contributionRepository = contributionRepository;
    }

    public List<Contribution> getAllContributions() {
        log.trace("Getting all contributions");
        ArrayList<Contribution> contributionArrayList = new ArrayList<>();
        contributionRepository.findAll().forEach(contributionArrayList::add);
        return contributionArrayList;
    }

    public boolean isExists(Integer id) {
        return getById(id).isPresent();
    }

    public Optional<Contribution> getById(int id) {
        return contributionRepository.findById(id);
    }

    public void addContribution(Contribution contribution) {
        log.trace("Saving contribution #" + contribution.getId());
        contributionRepository.save(contribution);
    }

    public void updateContribution(Contribution contribution, Integer contributionId) {
        log.trace("Updating contribution #" + contributionId);
        contribution.setId(contributionId);
        contributionRepository.save(contribution);
    }

    public void deleteContribution(Contribution contribution) {
        log.trace("Deleting contribution #" + contribution.getId());
        contributionRepository.delete(contribution);
    }

    public void deleteContribution(int id) {
        log.trace("Deleting contribution #" + id);
        contributionRepository.deleteById(id);
    }

}
