package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Role;
import by.bsuir.rekishbank.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class RoleService {

    private final String ROLE_PREFIX = "ROLE_";

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Map<Role, String> getRoleStringMap() {
        HashMap<Role, String> rolesNames = new LinkedHashMap<>();
        getAllRoles().forEach(x -> rolesNames.put(x, x.getName()));
        return rolesNames;
    }

    public List<Role> getAllRoles() {
        ArrayList<Role> roleArrayList = new ArrayList<>();
        roleRepository.findAll().forEach(roleArrayList::add);
        return roleArrayList;
    }

    public void addRole(Role role) {
        log.info("Creating new role " + role.getName());
        roleRepository.save(role);
    }

    public Role getByName(String name) {
        return roleRepository.findByName(name);
    }


    public void updateRole(Role role, Integer roleId) {
        log.info("Updating role " + role.getName());
        role.setId(roleId);
        roleRepository.save(role);
    }

    public void deleteRole(Role role) {
        log.info("Deleting role " + role.getName());
        roleRepository.delete(role);
    }

    public void deleteRole(int id) {
        log.info("Deleting role #" + id);
        roleRepository.deleteById(id);
    }


}
