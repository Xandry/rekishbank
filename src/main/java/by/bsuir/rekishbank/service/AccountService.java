package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Account;
import by.bsuir.rekishbank.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> getAllAccounts() {
        log.trace("Getting all accounts");
        ArrayList<Account> accountArrayList = new ArrayList<>();
        accountRepository.findAll().forEach(accountArrayList::add);
        return accountArrayList;
    }

    public boolean isExist(Integer accountId) {
        return getById(accountId).isPresent();
    }

    public Optional<Account> getById(Integer accountId) {
        log.trace("Getting account by id");
        return accountRepository.findById(accountId);
    }

    public void addAccount(Account account) {
        log.trace("Saving account to database");
        accountRepository.save(account);
    }

    public void setAccountName(String name, Integer idAccount) {
        log.trace("Changing account's name");
        Account account = getById(idAccount).get();
        account.setName(name);
        updateAccount(account);
    }

    public void changeAmount(Integer accountNumber, Double amount) {
        log.trace("Changing account't amount with account Number");
        changeAmount(getById(accountNumber).get(), amount);
    }

    public void changeAmount(Account account, Double amount) {
        log.trace("Changing account't amount with Account entity");
        account.setAccountAmount(account.getAccountAmount() + amount);
        updateAccount(account);
    }

    public void updateAccount(Account account, Integer accountId) {
        log.trace("Updating Account with entity and its id");
        account.setId(accountId);
        accountRepository.save(account);
    }

    public void updateAccount(Account account) {
        log.trace("Updating Account using entity");
        accountRepository.save(account);
    }

    public void deleteAccount(Account account) {
        log.trace("Deleting Account using entity");
        accountRepository.delete(account);
    }

    public void deleteAccount(int id) {
        log.trace("Deleting Account using id");
        accountRepository.deleteById(id);
    }


}
