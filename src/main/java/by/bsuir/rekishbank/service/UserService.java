package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.User;
import by.bsuir.rekishbank.entity.dto.UserDto;
import by.bsuir.rekishbank.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class UserService {

    private static final String ENCRYPTION_ALGORITHM = "{noop}";

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        ArrayList<User> userArrayList = new ArrayList<>();
        userRepository.findAll().forEach(userArrayList::add);
        return userArrayList;
    }

    public Map<User, String> getUserUsernameMap() {
        Map<User, String> usersNamesMap = new LinkedHashMap<>();
        getAllUsers().forEach(x -> {
            if (x.getClient() == null)
                usersNamesMap.put(x, x.getUsername());
        });
        return usersNamesMap;
    }

    public Optional<User> getById(Integer id) {
        return userRepository.findById(id);
    }

    public boolean isExists(Integer userId) {
        return getById(userId).isPresent();
    }

    public Optional<User> getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void addUser(User user) {
        log.trace("Creating user " + user.getUsername() + " as " + user.getRole().getName());
        user.setPasswordHash(ENCRYPTION_ALGORITHM + user.getPasswordHash());
        userRepository.save(user);
    }

    public void addUser(UserDto userDto) {
        log.trace("Creating user " + userDto.getUsername() + " as " + userDto.getRole().getName());
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPasswordHash(userDto.getPasswordHash());
        user.setRole(userDto.getRole());
        addUser(user);
    }

    public void updateUser(User user, Integer userId) {
        log.trace("Updating user " + user.getUsername() + " as " + user.getRole().getName());
        user.setId(userId);
        userRepository.save(user);
    }

    public void deleteUser(User user) {
        log.trace("Deleting user " + user.getUsername());
        userRepository.delete(user);
    }

    public void deleteUser(int id) {
        log.trace("Deleting user #" + id);
        userRepository.deleteById(id);
    }


}
