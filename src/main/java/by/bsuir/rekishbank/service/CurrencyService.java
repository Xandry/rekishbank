package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Currency;
import by.bsuir.rekishbank.repository.CurrencyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CurrencyService {

    private final CurrencyRepository currencyRepository;

    private final CurrencyHistoryService currencyHistoryService;

    public CurrencyService(CurrencyRepository currencyRepository, CurrencyHistoryService currencyHistoryService) {
        this.currencyRepository = currencyRepository;
        this.currencyHistoryService = currencyHistoryService;
    }

    public List<Currency> getAllCurrencies() {
        ArrayList<Currency> currencyArrayList = new ArrayList<>();
        currencyRepository.findAll().forEach(currencyArrayList::add);
        return currencyArrayList;
    }

    public boolean isExist(Integer id) {
        return getById(id).isPresent();
    }

    public Optional<Currency> getById(int id) {
        return currencyRepository.findById(id);
    }

    public void addCurrency(Currency currency) {
        log.trace("Creating currency " + currency.getName());
        currencyRepository.save(currency);
    }

    public void updateCurrency(Currency currency, Integer currencyId) {
        log.trace("Updating currency " + currency.getName());
        currency.setId(currencyId);
        currencyRepository.save(currency);
    }

    public void deleteCurrency(Currency currency) {
        log.trace("Deleting currency " + currency.getName());
        currencyRepository.delete(currency);
    }

    public void deleteCurrency(int id) {
        log.trace("Deleting currency id #" + id);
        currencyRepository.deleteById(id);
    }

    public double getRate(Currency currency) {
        return currencyHistoryService.getCurrencyRate(currency);
    }

}
