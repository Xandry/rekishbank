package by.bsuir.rekishbank.service;

import by.bsuir.rekishbank.entity.Currency;
import by.bsuir.rekishbank.entity.CurrencyHistory;
import by.bsuir.rekishbank.repository.HistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CurrencyHistoryService {

    private final HistoryRepository historyRepository;

    public CurrencyHistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    public List<CurrencyHistory> getAllHistory(Currency currency) {
        return new ArrayList<>(historyRepository.findAllByCurrency(currency));
    }

    public Optional<CurrencyHistory> getById(Integer id) {
        return historyRepository.findById(id);
    }

    public void addCurrencyHistory(CurrencyHistory currencyHistory) {
        log.trace("Creating new record for " + currencyHistory.getCurrency().getName());
        historyRepository.save(currencyHistory);
    }

    public void updateCurrencyHistory(CurrencyHistory currencyHistory) {
        log.trace("Updating currency history record");
        historyRepository.save(currencyHistory);
    }

    public void updateCurrencyHistory(CurrencyHistory currencyHistory, Integer historyId) {
        log.trace("Updating currency history record");
        currencyHistory.setId(historyId);
        historyRepository.save(currencyHistory);
    }

    public void deleteCurrencyHistory(Integer id) {
        log.trace("Deleting currency history record");
        historyRepository.deleteById(id);
    }

    public void deleteCurrencyHistory(CurrencyHistory currencyHistory) {
        log.trace("Deleting currency history record");
        historyRepository.delete(currencyHistory);
    }

    public void deleteCurrencyHistory(Currency currency) {
        log.trace("Deleting currency history record");
        historyRepository.deleteAllByCurrency(currency);
    }

    public double getCurrencyRate(Currency currency) {
        List<CurrencyHistory> currencyHistoryList = historyRepository.findAllByCurrency(currency);
        currencyHistoryList.sort((x, y) -> y.getDate().compareTo(x.getDate()));
        return currencyHistoryList.get(0).getRate();
    }
}
