package by.bsuir.rekishbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RekishBankApplication {


    public static void main(String[] args) {
        SpringApplication.run(RekishBankApplication.class, args);
    }

}
