<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 17:39
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <c:out value="${error}"/>
    </title>
</head>
<body>

<h2><c:out value="${error}"/></h2>
<br>
<a href="${pageContext.request.contextPath}/">
    <spring:message code="rekish-bank"/>
</a>

</body>
</html>
