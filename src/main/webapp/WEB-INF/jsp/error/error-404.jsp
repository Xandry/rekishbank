<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 16:20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="error"/></title>
</head>
<body>
<h2><spring:message code="error.404.name"/></h2>
<br>
<h3><spring:message code="error.404.code"/></h3>
<br>
<h4><spring:message code="error.404.description"/></h4>
</body>
</html>
