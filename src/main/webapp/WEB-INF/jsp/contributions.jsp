<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spi" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>
        <spring:message code="contributions"/>
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>
<body>

<%@ include file="header.jsp" %>

<script>
    function contributionCalculator() {
        // ifee - an initial fee
        // rate - interest rate
        // m - term in months

        if (!document.getElementById('interest_rate_input').checkValidity() ||
            !document.getElementById('term_input').checkValidity() ||
            !document.getElementById('initial_fee_input').checkValidity()) {
            return;
        }

        let m = document.getElementById('term_input').value;
        let ifee = document.getElementById('initial_fee_input').value;
        let rate = document.getElementById('interest_rate_input').value;


        let interestsPerMonth = [1];
        let multiplier = 1 + (rate / 1200);
        for (let i = 1; i <= m; i++) {
            let interestThisMonth = interestsPerMonth[interestsPerMonth.length - 1];
            interestThisMonth *= multiplier;
            interestsPerMonth.push(interestThisMonth);
        }

        var tbl = document.createElement('table');
        tbl.style.width = '100px';
        tbl.style.border = '1px solid black';

        let tr1 = tbl.insertRow();
        let td1 = tr1.insertCell();
        td1.appendChild(document.createTextNode("<spring:message code="contribution.month"/>"));
        let td2 = tr1.insertCell();
        td2.appendChild(document.createTextNode("<spring:message code="contribution.amount"/>"));
        td1.style.border = '1px solid black';
        td2.style.border = '1px solid black';


        for (var i = 1; i < interestsPerMonth.length; i++) {
            var tr = tbl.insertRow();
            for (var j = 0; j < 2; j++) {

                var td = tr.insertCell();
                if (j === 0) {
                    td.appendChild(document.createTextNode(i));
                } else {
                    var numb = interestsPerMonth[i] * ifee;
                    numb = +numb.toFixed(2);
                    td.appendChild(document.createTextNode(numb));
                }
                td.style.border = '1px solid black';

            }
        }

        document.getElementById('calculation_result').innerHTML = "";
        document.getElementById('calculation_result').append(tbl);

    }
</script>

<h2><spring:message code="contribution.list"/></h2>
<c:if test="${contributions.size() eq 0}">
    <h2><spring:message code="contribution.no-data"/></h2>
</c:if>
<c:if test="${contributions ne null || contributions.size() ne 0}">
    <table>
        <tr>
            <td>
                <spring:message code="contribution.name"/>
            </td>
            <td>
                <spring:message code="contribution.term"/>
            </td>
            <td>
                <spring:message code="contribution.interest-rate"/>
            </td>
        </tr>
        <c:forEach items="${contributions}" var="contr">
            <tr>
                <td>
                    <a href="${"/contributions/"}${contr.id}"><c:out value="${contr.name}"/></a>
                </td>
                <td>
                    <c:out value="${contr.term}"/>
                </td>
                <td>
                    <c:out value="${contr.interestRate}"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<div id="contribution_calculator">
    <h2>
        <spring:message code="contribution.calculator"/>
    </h2>
    <table>

        <tr>
            <td>
                <label for="interest_rate_input"/>
            </td>
            <td>
                <spring:message code="contribution.interest-rate"/>
            </td>
            <td>
                <input id="interest_rate_input" type="number" min="0" max="1000"
                       value="12" pattern="^[+]?[0-9]*\.?[0-9]+$">
            </td>


            <%--    </label>--%>
        </tr>
        <%--        <br>--%>
        <tr>
            <td>
                <label for="initial_fee_input"/>
            </td>
            <td>
                <spring:message code="contribution.initial-fee"/>
            </td>
            <td>
                <input id="initial_fee_input" type="number" max="100000" min="0"
                       value="1000" pattern="^[+]?[0-9]*\.?[0-9]+$">
            </td>

        </tr>
        <%--    </label>--%>
        <%--        <br>--%>
        <tr>
            <td>
                <label for="term_input"/>
            </td>
            <td>
                <spring:message code="contribution.term-in-month"/>
            </td>
            <td>
                <input id="term_input" type="number" value="12"
                       pattern="^[+]?[0-9]+$" min="1" max="100">
            </td>
        </tr>
        <%--    </label>--%>
        <%--        <br>--%>
    </table>
    <button onclick="contributionCalculator()">
        <spring:message code="contribution.calculate"/>
    </button>
    <div id="calculation_result"></div>
</div>

<sec:authorize access="hasRole('ADMIN')">

    <h2>
        <spring:message code="contribution.create"/>
    </h2>
    <form:form action="/admin/contributions" method="post" modelAttribute="contribution">
        <table>
            <tr>
                <td>
                    <spring:message code="contribution.name"/>
                </td>
                <td>
                    <form:input path="name" required="required"/>
                </td>
                <td>
                    <form:errors path="name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="contribution.term-in-month"/>
                </td>
                <td>
                    <form:input path="term" required="required"/>
                </td>
                <td>
                    <form:errors path="term"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="contribution.income-operations"/>
                </td>
                <td>
                    <form:checkbox path="incomeOperation"/>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="contribution.expense-operations"/>
                </td>
                <td>
                    <form:checkbox path="expenseOperation"/>
                </td>
                <td>

                </td>
            </tr>

        </table>
        <%--        <label>--%>
        <%--            <spring:message code="contribution.name"/>--%>
        <%--            <form:input path="name" required="required"/>--%>
        <%--            <form:errors path="name"/>--%>
        <%--        </label>--%>
        <%--        <br/>--%>
        <%--        <label>--%>
        <%--            <spring:message code="contribution.term-in-month"/>--%>
        <%--            <form:input path="term" required="required"/>--%>
        <%--            <form:errors path="term"/>--%>
        <%--        </label>--%>
        <%--        <br/>--%>
        <%--        <label>--%>
        <%--            <spring:message code="contribution.income-operations"/>--%>
        <%--            <form:checkbox path="incomeOperation"/>--%>
        <%--        </label>--%>
        <%--        <br/>--%>
        <%--        <label>--%>
        <%--            <spring:message code="contribution.expense-operations"/>--%>
        <%--            <form:checkbox path="expenseOperation"/>--%>
        <%--        </label>--%>
        <br/>
        <input type="submit" value="<spring:message code="submit"/>">
    </form:form>

</sec:authorize>

</body>
</html>