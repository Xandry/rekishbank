<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by 
  User: Федор
  Date: 02.12.2019
  Time: 19:49
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:if test="${account.name eq null}">
        <title><spring:message code="account"/></title>
    </c:if>
    <c:if test="${account.name ne null}">
        <title>${account.name}</title>
    </c:if>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test="${account.name eq null}">
    <h1><spring:message code="account"/></h1>
</c:if>
<c:if test="${account.name ne null}">
    <h1>${account.name}</h1>
</c:if>


<h2><spring:message code="account.amount"/> ${account.accountAmount}</h2>
<br>
<table>
    <tr>
        <td>
            <spring:message code="contribution.name"/>
        </td>
        <td>
            <c:out value="${account.contribution.name}"/>
        </td>
    </tr>
    <tr>
        <td>
            <spring:message code="account.open-date"/>
        </td>
        <td>
            <fmt:formatDate value="${account.openDate}" pattern="yyyy-MM-dd"/>
        </td>
    </tr>
    <tr>
        <td>
            <spring:message code="currency"/>
        </td>
        <td>
            <c:out value="${account.currency.name}"/>
        </td>
    </tr>
</table>
<br>

<c:if test="${account.card eq null}">
    <spring:message code="account.no-card"/>
    <form:form method="post" action="${'/cabinet/accounts/'}${account.id}${'/createCard'}">
        <input type="submit" value="<spring:message code="account.request-for-card"/>">
    </form:form>
</c:if>

<c:if test="${account.card ne null}">
    <h3><spring:message code="card"/></h3>
    <br>
    <label>
        <spring:message code="card.number"/>
        <c:out value="${account.card.cardNumber}"/>
    </label>
    <br>
    <label>
        <spring:message code="card.exp-date"/>
        <c:out value="${expdate}"/>
    </label>
    <br>


    <c:if test="${account.card.active}">
        <spring:message code="card.active"/>
    </c:if>
    <c:if test="${not account.card.active}">
        <spring:message code="card.blocked"/>
    </c:if>


    <br>
    <form:form action="${'/cabinet/accounts/'}${account.id}${'/changeCardStatus'}" method="post">
        <c:if test="${account.card.active}">
            <input type="submit" value="<spring:message code="card.block"/>">
        </c:if>
        <c:if test="${not account.card.active}">
            <input type="submit" value="<spring:message code="card.unblock"/>">
        </c:if>
    </form:form>


    <br>
    <form:form action="${'/cabinet/accounts/'}${account.id}${'/renewalApplication'}" method="post">
        <input type="submit" value="<spring:message code="card.renewal-application"/>">
    </form:form>
</c:if>


<form method="get" action="${pageContext.request.contextPath}/cabinet/accounts/${account.id}${'/setName'}">
    <input type="submit" value="<spring:message code="account.change-name"/>">
</form>

<%--Return to cabinet--%>
<form:form method="get" action="${'/cabinet'}">
    <input type="submit" value="<spring:message code="cabinet"/>">
</form:form>
</body>
</html>
