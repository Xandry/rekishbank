<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 02.12.2019
  Time: 1:13
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <spring:message code="registration.success"/>
    </title>
</head>
<body>

<%@ include file="header.jsp" %>

<h2>
    <spring:message code="registration.success"/>
</h2>
<br>
<h3><a href="${pageContext.request.contextPath}/">
    <spring:message code="homepage"/>
</a></h3>

</body>
</html>
