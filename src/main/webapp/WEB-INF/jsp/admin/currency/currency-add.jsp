<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 06.12.2019
  Time: 10:24
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title><spring:message code="currency.add"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="currency.add"/></h2>
<form:form action="/admin/currencies/add" method="post" modelAttribute="currency">
    <table>
        <tr>
            <td>
                <spring:message code="currency.name"/>
            </td>
            <td>
                <form:input path="name" required="required"/>
            </td>
            <td>
                <form:errors path="name"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="currency.rate-to-usd"/>
            </td>
            <td>
                <input name="rate" placeholder="<spring:message code="currency.rate-to-usd"/>" required="required">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br>
    <input type="submit" value="<spring:message code="submit"/>">
</form:form>

</body>
</html>
