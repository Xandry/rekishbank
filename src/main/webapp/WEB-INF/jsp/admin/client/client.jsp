<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title>${client.lastName} ${client.firstName}</title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h1>${client.lastName} ${client.firstName}</h1>
<br>
<label>
    <spring:message code="client.passport-number"/>
    <c:out value="${client.passportNumber}"/>
</label>
<br>
<label>
    <spring:message code="client.phone-number"/>
    <c:out value="${client.phoneNumber}"/>
</label>
<br>
<label>
    <spring:message code="client.address"/>
    <c:out value="${client.address}"/>
</label>
<br>


<h2>
    <spring:message code="accounts"/>
</h2>
<c:set var="zero" value="${0}"/>
<c:if test="${client.accountList.size() eq zero}">
    <spring:message code="client.no-accounts"/>
</c:if>

<c:if test="${client.accountList.size() ne zero}">
    <c:forEach items="${client.accountList}" var="account">
        <a href="${'/admin/accounts/'}${account.id}">
            <c:out value="${account.contribution.name} ${account.currency.name}"/>
        </a>
        <br>
    </c:forEach>
</c:if>

<br>
<%--<a href="/admin/clients/${client.id}${'/update'}">--%>
<%--    <spring:message code="update"/>--%>
<%--</a>--%>
<%--<br>--%>
<form action="/admin/clients/${client.id}${'/update'}" method="get">
    <input type="submit" value="<spring:message code="update"/>">
</form>

<form action="${'/admin/clients/delete'}">
    <input type="hidden" name="id" value="${client.id}">
    <input type="submit" value="<spring:message code="client.delete"/>">
</form>

</body>
</html>