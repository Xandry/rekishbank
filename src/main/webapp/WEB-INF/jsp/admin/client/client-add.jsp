<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 15:47
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title><spring:message code="client.create"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="client.create"/></h2>
<form:form action="/admin/clients/add" method="post" modelAttribute="client">
    <label>
        <spring:message code="client.first-name"/>
        <form:input path="firstName" required="required"/>
        <form:errors path="firstName"/>
    </label>
    <br>
    <label>
        <spring:message code="client.lastname"/>
        <form:input path="lastName" required="required"/>
        <form:errors path="lastName"/>
    </label>
    <br>
    <label>
        <spring:message code="client.passport-number"/>
        <form:input path="passportNumber" required="required"/>
        <form:errors path="passportNumber"/>
    </label>
    <br>
    <label>
        <spring:message code="client.phone-number"/>
        <form:input path="phoneNumber" required="required"/>
        <form:errors path="phoneNumber"/>
    </label>
    <br>
    <label>
        <spring:message code="client.address"/>
        <form:input path="address" required="required"/>
        <form:errors path="address"/>
    </label>
    <br>
    <label>
        <spring:message code="user"/>
        <form:select path="user" items="${users}" itemValue="id" required="required"/>
        <form:errors path="user"/>
    </label>
    <br>
    <input type="submit" value="<spring:message code="submit"/>">
    <br>
</form:form>

</body>
</html>
