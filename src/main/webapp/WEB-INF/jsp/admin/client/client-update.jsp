<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 16:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title><spring:message code="client.update"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="client.update"/></h2>
<form:form cssClass="labeled-input-fields" action="${'/admin/clients/'}${clientDto.id}${'/update'}" method="post"
           modelAttribute="clientDto">

    <table>
        <tr>
            <td>
                <spring:message code="client.first-name"/>
            </td>
            <td>
                <form:input path="firstName" required="required"/>
            </td>
            <td>
                <form:errors path="firstName"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="client.lastname"/>
            </td>
            <td>
                <form:input path="lastName" required="required"/>
            </td>
            <td>
                <form:errors path="lastName"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="client.passport-number"/>
            </td>
            <td>
                <form:input path="passportNumber" required="required"/>
            </td>
            <td>
                <form:errors path="passportNumber"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="client.phone-number"/>
            </td>
            <td>
                <form:input path="phoneNumber" required="required"/>
            </td>
            <td>
                <form:errors path="phoneNumber"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="client.address"/>
            </td>
            <td>
                <form:input path="address" required="required"/>
            </td>
            <td>
                <form:errors path="address"/>
            </td>
        </tr>
    </table>
    <%--    <label>--%>
    <%--        <spring:message code="client.first-name"/>--%>
    <%--        <form:input path="firstName" required="required"/>--%>
    <%--        <form:errors path="firstName"/>--%>
    <%--    </label>--%>
    <%--    <br>--%>
    <%--    <label>--%>
    <%--        <spring:message code="client.lastname"/>--%>
    <%--        <form:input path="lastName" required="required"/>--%>
    <%--        <form:errors path="lastName"/>--%>
    <%--    </label>--%>
    <%--    <br>--%>
    <%--    <label>--%>
    <%--        <spring:message code="client.passport-number"/>--%>
    <%--        <form:input path="passportNumber" required="required"/>--%>
    <%--        <form:errors path="passportNumber"/>--%>
    <%--    </label>--%>
    <%--    <br>--%>
    <%--    <label>--%>
    <%--        <spring:message code="client.phone-number"/>--%>
    <%--        <form:input path="phoneNumber" required="required"/>--%>
    <%--        <form:errors path="phoneNumber"/>--%>
    <%--    </label>--%>
    <%--    <br>--%>
    <%--    <label>--%>
    <%--        <spring:message code="client.address"/>--%>
    <%--        <form:input path="address" required="required"/>--%>
    <%--        <form:errors path="address"/>--%>
    <%--    </label>--%>
    <%--    <br>--%>
    <input type="submit" value="<spring:message code="submit"/>">

    <br>
</form:form>

</body>
</html>
