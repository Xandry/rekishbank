<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title><spring:message code="clients"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<c:if test="${clients eq null || clients.size() eq 0}">
    <spring:message code="client.no-data"/>
</c:if>

<c:if test="${clients ne null}">
    <h2><spring:message code="clients"/></h2>
</c:if>
<c:forEach items="${clients}" var="client">
    <a href="${'/admin/clients/'}${client.id}">
        <c:out value="${client.lastName} ${client.firstName}"/>
    </a>
    <br/>
</c:forEach>

<br>
<h3>
    <a href="/admin/clients/add"><spring:message code="client.create"/></a>
</h3>

</body>
</html>