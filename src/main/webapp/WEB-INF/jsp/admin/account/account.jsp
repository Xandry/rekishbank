<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${account.contribution.name} ${account.currency.name}</title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h1><spring:message code="account"/></h1>

<h2><label>
    <spring:message code="account.amount"/>
    <c:out value="${account.accountAmount}"/>
</label></h2>

<br>
<c:if test="${account.contribution ne null}">
    <label>
        <spring:message code="contribution.name-of-contr"/>
        <c:out value="${account.contribution.name}"/>
    </label>
    <br>
</c:if>
<label>
    <spring:message code="account.open-date"/>
    <c:out value="${account.openDate}"/>
</label>
<br>
<a href=${'/admin/clients/'}${account.client.id}>
    <label>
        <spring:message code="client"/>
        <c:out value="${account.client.lastName}${account.client.firstName}"/>
    </label>
</a>

<br>
<label>
    <spring:message code="currency"/>
    <c:out value="${account.currency.name}"/>
</label>
<br>

<c:if test="${account.card eq null}">
    <spring:message code="user.no-card-for-account"/>
</c:if>

<c:if test="${account.card ne null}">
    <h3>
        <spring:message code="card"/>
    </h3>
    <label>
        <spring:message code="card.number"/>
        <c:out value="${account.card.cardNumber}"/>
    </label>
    <br>
    <label>
        <spring:message code="card.exp-date"/>
        <c:out value="${expdate}"/>
    </label>


    <c:if test="${account.card.active}">
        <spring:message code="card.active"/>
    </c:if>
    <c:if test="${not account.card.active}">
        <spring:message code="card.blocked"/>
    </c:if>
    <br>
    <form:form action="${'/admin/accounts/'}${account.id}${'/changeCardStatus'}" method="post">
        <c:if test="${account.card.active}">
            <input type="submit" value="<spring:message code="card.block"/>">
        </c:if>
        <c:if test="${not account.card.active}">
            <input type="submit" value="<spring:message code="card.unblock"/>">
        </c:if>

    </form:form>
</c:if>

<form method="get" action="${'/admin/accounts/delete'}">
    <input type="hidden" name="id" value="${account.id}">
    <input type="submit" value="<spring:message code="account.delete"/>">
</form>


</body>
</html>