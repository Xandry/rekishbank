<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="accounts"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="accounts"/></h2>
<c:if test="${accounts eq null || accounts.size() eq 0}">
    <spring:message code="account.no-data"/>
</c:if>
<c:forEach items="${accounts}" var="account">
    <a href="${'/admin/accounts/'}${account.id}">
        <c:out value="${account.contribution.name} ${account.currency.name}"/>
    </a>
    <br>
</c:forEach>
<br>

<a href="${pageContext.request.contextPath}/admin/accounts/add">
    <spring:message code="account.create"/>
</a>

</body>
</html>