<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 21:35
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="account.create"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="account.create"/></h2>

<form:form action="/admin/accounts/add" method="post" modelAttribute="account">
    <label>
        <spring:message code="account.open-date"/>
        <form:input path="openDate" type="date" required="required"/>
        <form:errors path="openDate"/>
    </label>
    <br>
    <label>
        <spring:message code="account.amount"/>
        <form:input path="accountAmount" required="required" value="0"/>
        <form:errors path="accountAmount"/>
    </label>
    <br>
    <label>
        <spring:message code="client"/>
        <form:select path="client" items="${clients}" itemValue="id" itemLabel="lastName"/>
        <form:errors path="client"/>
    </label>
    <br>
    <label>
        <spring:message code="currency"/>
        <form:select path="currency" items="${currencies}" itemValue="id" itemLabel="name"/>
        <form:errors path="currency"/>
    </label>
    <br>
    <label>
        <spring:message code="contribution"/>
        <form:select path="contribution" items="${contributions}" itemValue="id" itemLabel="name"/>
        <form:errors path="contribution"/>
    </label>
    <br>
    <input type="submit" value="<spring:message code="submit"/>">

</form:form>

</body>
</html>
