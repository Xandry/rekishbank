<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype>
<html>
<head>
    <title>${user.username}</title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h1>${user.username}</h1>
<label>
    <spring:message code="user.password"/>
    <c:out value="${user.passwordHash}"/>
</label>
<br>
<label>
    <spring:message code="user.role"/>
    <c:out value="${user.role.name}"/>
</label>
<br>
<c:if test="${user.client ne null}">
    <a href="${'/admin/clients/'}${user.client.id}">
        <spring:message code="user.client"/>
    </a>
    <br>
</c:if>

<c:if test="${user.client eq null}">
    <spring:message code="user.no-client"/>
    <br>
</c:if>

<br>
<form method="get" action="${'/admin/users/delete'}">
    <input type="hidden" name="id" value="${user.id}">
    <input type="submit" value="<spring:message code="user.delete"/>">

</form>

</body>
</html>