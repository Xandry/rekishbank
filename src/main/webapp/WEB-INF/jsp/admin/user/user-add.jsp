<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 06.12.2019
  Time: 2:20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype>
<html>
<head>
    <title><spring:message code="user.add"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<h2><spring:message code="user.add"/></h2>
<form:form action="/admin/users/add" method="post" modelAttribute="user">
    <label>
        <spring:message code="user.username"/>
        <form:input path="username" required="required"/>
        <form:errors path="username"/>
    </label>
    <br/>
    <label>
        <spring:message code="user.password"/>
        <form:input path="passwordHash" required="required"/>
        <form:errors path="passwordHash"/>
    </label>
    <br/>
    <label>
        <spring:message code="user.role"/>
        <form:select path="role" items="${roles}" itemValue="id" required="required"/>
    </label>
    <input type="submit" value="<spring:message code="submit"/>">
</form:form>


</body>
</html>
