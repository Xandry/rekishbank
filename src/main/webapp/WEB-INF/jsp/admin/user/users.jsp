<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype>
<html>
<head>
    <title><spring:message code="users"/></title>
</head>
<body>

<%@ include file="../../header.jsp" %>

<c:if test="${users eq null || users.size() eq 0}">
    <spring:message code="users.no-data"/>
</c:if>
<c:if test="${users ne null || users.size() ne 0}">
    <h2><spring:message code="users"/></h2>
    <table>
        <tr>
            <td>
                <spring:message code="user.username"/>
            </td>
            <td>
                <spring:message code="user.role"/>
            </td>
        </tr>
        <c:forEach items="${users}" var="user">
            <tr>
                <td>
                    <label>
                        <a href="${"/admin/users/"}${user.id}">
                            <c:out value="${user.username}"/>
                        </a>
                    </label>
                </td>
                <td>
                    <label>
                        <c:out value="${user.role.name}"/>
                    </label>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<br>
<a href="${pageContext.request.contextPath}/admin/users/add">
    <spring:message code="user.add"/>
</a>

</body>
</html>