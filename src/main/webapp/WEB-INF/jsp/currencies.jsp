<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="currency.rates"/></title>
    <script>
        function calculate() {
            let rate1 = document.getElementById("cur_from").value;
            let rate2 = document.getElementById("cur_to").value;
            if (!document.getElementById("amount_from").checkValidity()) {
                document.getElementById("result").value = '<spring:message code="input-is-not-valid"/>';
            } else {
                let usd = document.getElementById("amount_from").value * rate1;
                document.getElementById("result").value = (usd / rate2).toFixed(2);
            }
        }
    </script>
</head>
<body>

<%@ include file="header.jsp" %>

<h2><spring:message code="currency.rates"/></h2>
<c:if test="${currencies eq null || currencies.size() eq 0}">
    <spring:message code="currency.no-data"/>
</c:if>
<c:forEach items="${currencies}" var="currency">
    <a href="${"/currencies/"}${currency.id}"><c:out value="${currency.name}"/></a>
    <c:out value="${rates[currency]}"/>
    <br/>
</c:forEach>


<div>
    <h2>
        <spring:message code="currency.converter"/>
    </h2>
    <select id="cur_from">
        <c:forEach items="${currencies}" var="currency">
            <option value="${rates[currency]}">${currency.name}</option>
        </c:forEach>
    </select>
    <input id="amount_from" pattern="^[+]?[0-9]*\.?[0-9]+$">
    &rarr;
    <select id="cur_to">
        <c:forEach items="${currencies}" var="currency">
            <option value="${rates[currency]}">${currency.name}</option>
        </c:forEach>
    </select>
    <input id="result" readonly>
    <input type="button" value="<spring:message code="submit"/>" onclick="calculate()">
</div>

<sec:authorize access="hasRole('ADMIN')">
    <br>
    <a href="/admin/currencies/add">
        <spring:message code="currency.add"/>
    </a>
</sec:authorize>

</body>
</html>