<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by
  User: Федор
  Date: 02.12.2019
  Time: 1:00
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="registration"/></title>
</head>
<body>


<form:form action="/registration" method="post" modelAttribute="user">
    <table>
        <tr>
            <td>
                <spring:message code="registration.login"/>
            </td>
            <td>
                <form:input path="username" required="required"/>
            </td>
            <td>
                <form:errors path="username"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="registration.password"/>
            </td>
            <td>
                <form:input type="password" path="passwordHash" required="required"/>
            </td>
            <td>
                <form:errors path="passwordHash"/>
            </td>
        </tr>
    </table>
    <%--    <label>--%>
    <%--        <spring:message code="registration.login"/>--%>
    <%--        <form:input path="username" required="required"/>--%>
    <%--        <form:errors path="username"/>--%>
    <%--    </label>--%>
    <%--    <br/>--%>
    <%--    <label>--%>
    <%--        <spring:message code="registration.password"/>--%>
    <%--        <form:input type="password" path="passwordHash" required="required"/>--%>
    <%--        <form:errors path="passwordHash"/>--%>
    <%--    </label>--%>
    <%--    <br/>--%>
    <input id="submit" type="submit" value="<spring:message code="submit"/>">
</form:form>

</body>
</html>
