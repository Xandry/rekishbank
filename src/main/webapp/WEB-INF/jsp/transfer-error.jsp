<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 05.12.2019
  Time: 1:23
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <spring:message code="transfer.error"/>
    </title>
</head>
<body>

<%@ include file="header.jsp" %>

<h2>
    <spring:message code="transfer.error"/>
</h2>

<br>
<a href="${pageContext.request.contextPath}/transfer">
    <spring:message code="return"/>
</a>

</body>
</html>
