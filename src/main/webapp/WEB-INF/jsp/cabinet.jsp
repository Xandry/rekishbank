<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 02.12.2019
  Time: 2:28
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="cabinet"/></title>
</head>
<body>

<%@ include file="header.jsp" %>

<h2><spring:message code="cabinet"/></h2>

<label>
    <spring:message code="me.username"/>
    <c:out value="${me.username}"/>
</label>

<br>
<label>
    <spring:message code="me.role"/>
    <c:out value="${me.role}"/>
</label>


<c:if test="${me.client eq null}">
    <h2><spring:message code="me.not-a-client"/></h2>
    <h3><spring:message code="fill-in-form"/></h3>
    <br>
    <form:form action="/cabinet/create-client" method="post" modelAttribute="client">

        <table>
            <tr>
                <td>
                    <spring:message code="client.first-name"/>
                </td>
                <td>
                    <form:input path="firstName" required="required"/>
                </td>
                <td>
                    <form:errors path="firstName"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="client.lastname"/>
                </td>
                <td>
                    <form:input path="lastName" required="required"/>
                </td>
                <td>
                    <form:errors path="lastName"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="client.passport-number"/>
                </td>
                <td>
                    <form:input path="passportNumber" required="required"/>
                </td>
                <td>
                    <form:errors path="passportNumber"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="client.phone-number"/>
                </td>
                <td>
                    <form:input path="phoneNumber" required="required"/>
                </td>
                <td>
                    <form:errors path="phoneNumber"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="client.address"/>
                </td>
                <td>
                    <form:input path="address" required="required"/>
                </td>
                <td>
                    <form:errors path="address"/>
                </td>
            </tr>
        </table>

        <input type="submit" value="<spring:message code="submit"/>">
        <br>
    </form:form>
</c:if>
<c:if test="${me.client ne null}">

    <h2>
        <spring:message code="me.info"/>
    </h2>
    <h3>${me.client.lastName} ${me.client.firstName}</h3>
    <br>
    <label>
        <spring:message code="client.passport-number"/>
        <c:out value="${me.client.passportNumber}"/>
    </label>
    <br>
    <label>
        <spring:message code="client.phone-number"/>
        <c:out value="${me.client.phoneNumber}"/>
    </label>
    <br>
    <label>
        <spring:message code="client.address"/>
        <c:out value="${me.client.address}"/>
    </label>
    <br>

    <c:if test="${me.client.accountList.size() eq 0}">
        <h2><spring:message code="me.no-accounts"/></h2>
    </c:if>


    <c:if test="${me.client.accountList.size() ne 0}">
        <h2><spring:message code="me.accounts"/></h2>
        <c:forEach items="${me.client.accountList}" var="account">
            <a href="${'/cabinet/accounts/'}${account.id}">
                <c:if test="${account.name eq null}">
                    <c:out value="${account.contribution.name} ${account.currency.name}"/>
                </c:if>
                <c:if test="${account.name ne null}">
                    <c:out value="${account.name} ${account.currency.name}"/>
                </c:if>

            </a>
            <br>
        </c:forEach>
    </c:if>


    <h2><spring:message code="account.create"/></h2>
    <form:form action="/cabinet/accounts" method="post" modelAttribute="account">

        <table>
            <tr>
                <td>
                    <spring:message code="currency"/>
                </td>
                <td>
                    <form:select path="currency" items="${currencies}" itemValue="id" itemLabel="name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <spring:message code="contribution"/>
                </td>
                <td>
                    <form:select path="contribution" items="${contributions}" itemValue="id" itemLabel="name"/>
                </td>
            </tr>
        </table>

        <input type="submit" value="<spring:message code="submit"/>">

    </form:form>


</c:if>


<br>
<h3><a href="${pageContext.request.contextPath}/">
    <spring:message code="homepage"/>
</a></h3>


</body>
</html>
