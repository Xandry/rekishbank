<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${currency.name}</title>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['<spring:message code="currency.date"/>', '<spring:message code="currency.rate-to-usd"/>'],

                <c:forEach items="${rates}" var="rate">
                [new Date(<c:out value="${rate.date.time}"/>), <c:out value="${rate.rate}"/>],
                </c:forEach>
            ]);

            var options = {
                title: '${currency.name} <spring:message code="currency.rate-to-usd"/>',
                curveType: 'function',
                legend: {position: 'bottom'}
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>

</head>
<body>

<%@ include file="header.jsp" %>

<h2>${currency.name} - ${currentRate}$</h2>

<div id="chart_div" style="width: 900px; height: 500px"></div>

<sec:authorize access="hasRole('ADMIN')">
    <form method="get" action="${'/admin/currencies/delete/'}">
        <input type="hidden" name="id" value="${currency.id}">
        <input type="submit" value="<spring:message code="currency.delete"/>">
    </form>

    <form:form method="post" action="${'/admin/currencies/'}${currency.id}${'/addRate'}">
        Add currency: <input name="value" required="required"/>
        <input type="submit" value="<spring:message code="submit"/>"/>
    </form:form>

</sec:authorize>

</body>
</html>