<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by 
  User: Федор
  Date: 07.12.2019
  Time: 13:46
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>header</title>
    <link rel="stylesheet" type="text/css" href="/css/header.css"/>
</head>
<body>
<header>
    <div class="header_container">
        <div class="logo_container">
            <img src="/images/logo.png" height="100" width="100" alt="REKISH BANK">
            <div>
                <a class="locale_select" href="?lang=ru">ru</a>
                <a class="locale_select" href="?lang=en">en</a>
            </div>
        </div>

        <div class="links_container">

            <div class="link">
                <a href="${pageContext.request.contextPath}/currencies">
                    <spring:message code="currency"/>
                </a>
            </div>
            <div class="link">
                <a href="${pageContext.request.contextPath}/contributions">
                    <spring:message code="contributions"/>
                </a>
            </div>
            <div class="link">
                <a href="${pageContext.request.contextPath}/transfer">
                    <spring:message code="transfer"/>
                </a>
            </div>

            <sec:authorize access="hasRole('ADMIN')">
                <div class="link">
                    <a href="${pageContext.request.contextPath}/admin/users">
                        <spring:message code="users"/>
                    </a>
                </div>
                <div class="link">
                    <a href="${pageContext.request.contextPath}/admin/clients">
                        <spring:message code="clients"/>
                    </a>
                </div>
                <div class="link">
                    <a href="${pageContext.request.contextPath}/admin/accounts">
                        <spring:message code="accounts"/>
                    </a>
                </div>
            </sec:authorize>

        </div>

        <div id="authorize_container" class="authorize_container">

            <sec:authorize access="!isAuthenticated()">
                <div class="link">

                    <a href="${pageContext.request.contextPath}/registration">
                        <spring:message code="registration"/>
                    </a>

                </div>
                <div class="link">
                    <a href="${pageContext.request.contextPath}/login">
                        <spring:message code="log-in"/>
                    </a>
                </div>
            </sec:authorize>


            <sec:authorize access="isAuthenticated()">
                <div class="link">
                        <%--                    <form:form method="get" action="${'/cabinet'}">--%>
                        <%--                        <input class="text-button" type="submit" value="<spring:message code="cabinet"/>">--%>
                    <a href="${'/cabinet'}">
                        <spring:message code="cabinet"/>
                    </a>
                        <%--                    </form:form>--%>
                </div>
                <div class="link">
                    <form:form method="post" action="${'/logout'}">
                        <input class="text-button" type="submit" value="<spring:message code="logout"/>">
                    </form:form>
                        <%--                    <a href="${'/logout'}">--%>
                        <%--                        <spring:message code="logout"/>--%>
                        <%--                    </a>--%>
                </div>
            </sec:authorize>
        </div>
    </div>

</header>

</body>
</html>
