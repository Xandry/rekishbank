<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${contribution.name}</title>
</head>
<body>

<%@ include file="header.jsp" %>

<h1>${contribution.name}</h1>
<label>
    <spring:message code="contribution.term-in-month"/>
    <c:out value="${contribution.term}"/>
</label>
<br>
<label>
    <spring:message code="contribution.interest-rate"/>
    <c:out value="${contribution.interestRate}"/>
</label>
<br>

<c:if test="${contribution.incomeOperation}">
    <spring:message code="contribution.income-operations.available"/>
    <br>
</c:if>

<c:if test="${contribution.expenseOperation}">
    <spring:message code="contribution.expense-operations.available"/>
    <br>
</c:if>
<br>

<sec:authorize access="hasRole('ADMIN')">
    <form action="${'/admin/contributions/delete'}">
        <input type="hidden" name="id" value="${contribution.id}">
        <input type="submit" value="<spring:message code="contribution.delete"/>">
    </form>
</sec:authorize>

</body>
</html>