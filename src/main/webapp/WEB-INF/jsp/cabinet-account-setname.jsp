<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 06.12.2019
  Time: 21:22
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="account.set-name"/></title>
</head>
<body>

<%@ include file="header.jsp" %>

<form:form action="${'/cabinet/accounts/'}${accountId}${'/setName'}" modelAttribute="account" method="post">
    <label>
        <spring:message code="account.set-name"/>
        <input name="name" required="required" type="text" minlength="1" maxlength="45"/>
        <form:errors path="name"/>
    </label>
    <br>
    <input type="submit" value="<spring:message code="submit"/>">
</form:form>

</body>
</html>
