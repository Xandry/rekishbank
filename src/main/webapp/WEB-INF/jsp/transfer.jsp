<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by 
  User: Федор
  Date: 04.12.2019
  Time: 22:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <spring:message code="transfer"/>
    </title>
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        body {
            width: 100%;
            height: 100%;
        }

        main {
            width: 100%;
            height: 80%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .label {
            font-size: 20px;
            margin-bottom: 20px;
        }

        .main_container {
            height: 100%;
            width: 800px;
            margin-top: 50px;
            margin-bottom: 50px;
            margin-left: 200px;
            margin-right: 200px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: center;

        }

        .transaction_form {
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            align-items: center;
        }

        .cards_form {
            width: 90%;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
        }


        input[type=number] {
            -moz-appearance: textfield;
        }

        .card_form {
            height: min-content;
            width: min-content;
            border: black;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: center;
            /*border-style: dashed;*/
        }

        .card {
            height: 5.4cm;
            width: 8.6cm;
            border-radius: 20px;
            /*border: #adadad;*/
            border-style: solid;
            border-width: 2px;
            border-color: #00adb5;
        }

        input {
            font-size: 20px;
            color: #474747;
            border: #969696;
            border-radius: 5px;
            border-style: solid;
            border-width: 1px;

        }

        .card_number, .expdate, .cvc, .cardholder {
            margin-left: 50px;
        }

        .card_number {
            padding: 2px;
            padding-left: 11px;
            margin-bottom: 20px;
            margin-top: 65px;
        }

        .expdate {
            width: 60px;
            padding-left: 4px;
            padding-right: 4px;
        }

        .cvc {
            width: 45px;
            padding-left: 4px;
            padding-right: 4px;
        }

        .cardholder {
            margin-top: 10px;
            padding-left: 4px;
        }


    </style>
</head>
<body>

<%@ include file="header.jsp" %>

<main>
    <div class="main_container">
        <div class="label">
            <spring:message code="transfer"/>
        </div>


        <form:form cssClass="transaction_form" action="/transfer" method="post">

            <div class="cards_form">

                <div class="card_from_form card_form">
                    <h4>
                        <spring:message code="transfer.from"/>
                    </h4>
                    <div class="card">
                        <input class="card_number" name="card_number_from"
                            <%--                               pattern="^[0-9]{16}$"--%>
                               placeholder="0000 0000 0000 0000" type="number" maxlength="16">
                        <br>
                        <input class="expdate" name="expdate" placeholder="05/21"
                               pattern="^[0-9]{1,2}/[0-9]{2}$" maxlength="5">
                        <input class="cvc" name="cvc" placeholder="123"
                               pattern="^[0-9]{3}$" maxlength="3">
                        <br>
                        <input class="cardholder" type="text" name="cardholder" placeholder="JOHN SMITH">

                    </div>
                </div>

                <div class="card_to_form card_form">
                    <h4>
                        <spring:message code="transfer.to"/>
                    </h4>
                    <div class="card">
                        <input class="card_number" name="card_number_to"
                            <%--                               pattern="^[0-9]{16}$"--%>
                               placeholder="0000 0000 0000 0000" type="number" maxlength="16">
                    </div>

                </div>

            </div>


            <div class="amount_field">
                <input class="amount" name="amount" max="1000" min="0"> USD
            </div>

            <div class="submit button">
                <input type="submit" value="<spring:message code="submit"/>">
            </div>

        </form:form>
    </div>
</main>

</body>
</html>
